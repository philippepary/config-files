# Hommage à Terry Pratchet

See <http://www.gnuterrypratchett.com>
    <IfModule headers_module>
        header set X-Clacks-Overhead "GNU Terry Pratchett"
    </IfModule>

# Signature et Token: Limitation de l’identification de la version du serveur

    ServerSignature Off : sur les pages d’erreur
    ServerTokens Prod : minimum d’info dans les header sur le serveur (version, modules etc.)

# HSTS: Pour SSL, contraindre l’usage

    Header always set Strict-Transport-Security "max-age=63072000; includeSubDomains"

# XFO: Ne pas permettre l’inclusion du site comme iframe (hameçonage)

    Header always set x-frame-options: SAMEORIGIN

# XSS: faille XSS. On ne communique pas avec du code issu de l’extérieur

    Header always set X-XSS-Protection "1; mode=block"

# X-Content-type: Bloquer certains navigateurs de définir eux-même le content-type

Certains navigateurs *sniffent* le content-type des données envoyées et utilisent le type constaté. Par exemple, si un attaquant réussi à remplacer une page HTML par un script JS, le script JS sera affiché comme une page web.

    Header always set X-Content-Type-Options: nosniff

# Cookies: limiter la possiblité de vol de session

Lire <https://blog.codinghorror.com/protecting-your-cookies-httponly/>

    Header edit Set-Cookie ^(.*)$ $1;HttpOnly;Secure

# CSP: Sources autorisées pour les scripts

    Header unset Content-Security-Policy
    Header add Content-Security-Policy "default-src 'self'"

À adapter avec les sources externes nécessaires au site web. Voir <https://content-security-policy.com/>

# Ressources

* <https://geekflare.com/apache-web-server-hardening-security>
* <https://observatory.mozilla.org/>
